require 'rnn'
require 'torch'
require 'hdf5'
require 'optim'
require 'nn'
require 'eval'
require 'LinearO'
local color = require 'trepl.colorize'
local utils = require 'utils'
local json = require 'json'
local weight_init = require 'weight_init'

cmd = torch.CmdLine()
cmd:text()
cmd:text('Options for the context model')
cmd:option('-hid', 500, 'units in the hidden layer')
cmd:option('-nhid', 1, 'number of hidden layers')
cmd:option('-bsize', 64, 'batch size while training')
cmd:option('-epochs', 60, 'number of epochs to train for')
cmd:option('-exp', 'new_exp', 'experiment directory')
cmd:option('-fnf', 'f', 'Type of features to use: classification(nf) or detection(f)')
cmd:option('-lr', 0.01, 'Learning rate for training')
cmd:option('-gpuid', 1, 'GPU to use. Set 0 to use CPU')
cmd:option('-seed', 123, 'Random seed to standardize runs')
cmd:option('-set', 'coco', 'Dataset to work on')
cmd:option('-rel', 1, 'Whether to add relu after last layer')
cmd:option('-method', 'heuristic', 'Method for initializing the weights')
cmd:option('-estop', 20, 'Number of epochs for early-stopping')
cmd:option('-checkpt', 20, 'Number of epochs after which every model is to be saved')
cmd:option('-wt_dec', 0.01, 'Weight Decay')
cmd:option('-lr_dec', 0.3, 'Learning Rate Decay')
cmd:option('-decay_every', 15, 'Decay Learning Rate every n epochs')
cmd:option('-optimizer', 'rmsprop', 'sgd|rmsprop|adagrad|asgd|adadelta|adam|nag')

opt = cmd:parse(arg)

local exp_dir = opt.exp .. '_rnn_' .. opt.set .. '_' .. opt.fnf .. '_' .. tostring(opt.nhid) .. 'hl_' .. tostring(opt.hid) .. '_' .. opt.optimizer .. '_rel_' .. tostring(opt.rel) .. '_lr_' .. tonumber(opt.lr)

paths.mkdir(exp_dir)

--Save experiment config
local exp_config = {}

exp_config['hid_layer_size'] = opt.hid
exp_config['num_hid'] = opt.nhid
exp_config['type_f'] = opt.fnf
exp_config['optimizer'] = opt.optimizer
exp_config['lr'] = opt.lr
exp_config['lr_decay'] = opt.lr_dec
exp_config['wt_dec'] = opt.wt_dec
exp_config['lr_decay_every'] = opt.decay_every
exp_config['used_relu'] = opt.rel
exp_config['w_init'] = opt.method
exp_config['batchsize'] = opt.bsize


local table_json = json.encode(exp_config)
print(exp_dir)
print('-----')
print('-----')
print('-----')
print(table_json)

local file = io.open(exp_dir .. '/exp_config.json', 'w')

if file then
  file:write(table_json)
  io.close(file)
end


--Set defaults
torch.setdefaulttensortype('torch.DoubleTensor')
torch.manualSeed(opt.seed)

if opt.gpuid > 0 then
	require 'cutorch'
	require 'cunn'
	cutorch.setDevice(1)
end

 local data_dir = '/srv/share/vqa_counting/conv3/vgg16/h5_data/' .. opt.set .. '/'

 if type_f == 'nf' then
	 pret_cnn = 'vgg'
 else
	 pret_cnn = 'vggdet'
 end

local train_path = data_dir .. 'train_aso-sub_3_' .. pret_cnn .. '.h5'
local val_path = data_dir .. 'val_aso-sub_3_' .. pret_cnn ..'.h5'
local test_path = data_dir .. 'test_aso-sub_3_' .. pret_cnn .. '.h5'

local train_file = hdf5.open(train_path, 'r')
local val_file = hdf5.open(val_path, 'r')
local test_file = hdf5.open(test_path, 'r')

X_train = train_file:read('/data'):all()
X_val = val_file:read('/data'):all()
X_test = test_file:read('/data'):all()
Y_train = train_file:read('/label'):all()
Y_val = val_file:read('/label'):all()
Y_test = test_file:read('/label'):all()

-----------------------
ncat = Y_train:size(2)
net_ip_size = X_train:size(2)
print('Data Loaded..')


--Prepare data in sequential form
local function prepare_sequence(mat, count)
	local temp_data = torch.zeros(mat:size(1)/9, 9, mat:size(2))
	local temp_count = torch.zeros(count:size(1)/9, 9, count:size(2))
	for i=1,mat:size(1)/9 do
		temp_data[i] = mat:sub(9*i-8,9*i)
		temp_count[i] = count:sub(9*i-8,9*i)
	end
	return temp_data, temp_count
end

X_aso_train, Y_aso_train = prepare_sequence(X_train, Y_train)
X_aso_val, Y_aso_val = prepare_sequence(X_val, Y_val)
X_aso_test, Y_aso_test = prepare_sequence(X_test, Y_test)
--Design RNN model

model = nn.Sequential()
model:add(nn.Sequencer(nn.Linear(net_ip_size, opt.hid)))
rnn = nn.Recurrent(opt.hid, nn.Linear(opt.hid, opt.hid), nn.Linear(opt.hid, opt.hid), nn.HardTanh(), 5)
model:add(nn.Sequencer(rnn))
model:add(nn.Sequencer(nn.Linear(opt.hid, ncat)))

if opt.rel == 1 then
	model:add(nn.Sequencer(nn.ReLU()))
end

print(model)

criterion = nn.SequencerCriterion(nn.MSECriterion())
if opt.gpuid > 0 then
	model:cuda()
	criterion:cuda()
end
model = weight_init(model, opt.method)

param, gparam = model:getParameters()
--Optimizer
optimState = {
  learningRate = opt.lr,
  weightDecay = opt.wt_dec,
  learningRateDecay = opt.lr_dec
}

ep_step = opt.decay_every

local tr_loss = {}
local vl_loss = {}
local tr_loss_iter = {}
local logger = optim.Logger(exp_dir .. '/counting_cnn_epoch.log')
logger:setNames{'Training Error', 'Validation Error'}

local epoch = 1
local iter = 1
local lr = opt.lr
print('Experiment Configured')
local function train()
	local time = sys.clock()
	model:training()
	local shuffle = torch.randperm(X_aso_train:size(1))
	--if epoch ~= 0 and epoch%ep_step == 0 then lr = lr*opt.lr_dec end	
	if epoch ~= 0 and epoch % ep_step == 0 and optimizer ~= 'adagrad' then optimState.learningRate = optimState.learningRate*optimState.learningRateDecay end
	local train_losses = {}
	-- Create mini-batches on the fly
	local Xt = torch.Tensor(opt.bsize, X_aso_train:size(2), X_aso_train:size(3))
	local Yt = torch.Tensor(opt.bsize, Y_aso_train:size(2), Y_aso_train:size(3))
	for it = 1, X_aso_train:size(1), opt.bsize do
		if (it + opt.bsize - 1) > X_aso_train:size(1) then
			break
		end
		local idx = 1
		for i=it, it+opt.bsize-1 do
			Xt[idx] = X_aso_train[shuffle[i]]
			Yt[idx] = Y_aso_train[shuffle[i]]
		end
		X_t = Xt:reshape(Xt:size(2), Xt:size(1), Xt:size(3))
		Y_t = Yt:reshape(Yt:size(2), Yt:size(1), Yt:size(3))
		if opt.gpuid > 0 then
			X_t = X_t:cuda()
			Y_t = Y_t:cuda()
		end
		local input, target = {}, {}
		for i=1, X_t:size(1) do
			table.insert(input, X_t[i])
			table.insert(target, Y_t[i])
		end
		local feval = function(x)
			if x ~= param then param:copy(x) end
			model:zeroGradParameters()
			local output = model:forward(input)
			local iter_loss = criterion:forward(output, target) 
			table.insert(train_losses, iter_loss)
			local gradOutputs = criterion:backward(output, target)
			local gradInput = model:backward(input, gradOutputs)
			for i=1,#gradInput do
				local size = gradInput[i]:nDimension()
				gradInput[i]:clamp(-1/size,1/size)
			end
			return iter_loss, gparam
		end
    if optimizer == 'rmsprop' then
      optim.rmsprop(feval, param, optimState)
    else
      optim.adagrad(feval, param, optimState)
    end
		iter = iter + 1
	end
	time = sys.clock() - time
	print(color.red'Epoch: ' .. epoch .. color.blue' training loss: ' .. utils.table_mean(train_losses) .. color.blue' Learning Rate: ' .. lr .. color.blue' Time: ' .. time .. '    s')
	epoch = epoch + 1
	return utils.table_mean(train_losses)
end

local evaluation_count = eval_count(1,1)
local function test(model_test, split)
	model_test:evaluate()
	local all_counts = {}
	local Y_req = torch.Tensor()
	if split == 'val' then
		X_req = X_aso_val:clone()
		Y_req = Y_aso_val:clone()
	else
		X_req = X_aso_test:clone()
		Y_req = Y_aso_test:clone()
	end
	local Y_pred = torch.zeros(Y_req:size(1), Y_req:size(3))
	if opt.gpuid >0 then
		X_req = X_req:cuda()
		Y_req = Y_req:cuda()
		Y_pred = Y_pred:cuda()
	end
	for its = 1, X_req:size(1) do 
		local input = {}
		for j=1,X_req[its]:size(1) do
			table.insert(input, X_req[its][j])
		end
		local count_pred = model_test:forward(input)
		for k=1,#count_pred do
			local a = count_pred[k]
			Y_pred[its] = Y_pred[its] + a:cmax(0)
		end
	end
	local mse = 0
	local mrmse = 0
	local mmae = 0
	local Y_gt = torch.squeeze(Y_req:sum(2))
	if split == 'val' then
  	mse = evaluation_count:mse(Y_pred, Y_gt)
		mrmse = evaluation_count:mrmse(Y_pred, Y_gt)
  	mmae = evaluation_count:mmae(Y_pred, Y_gt)
  	print(string.format('Test Count Loss: split(%s)  mrmse: %f, mmae: %f, mse: %f', split, mrmse, mmae, mse))
	else
    mse, mse_std = evaluation_count:eval_for_paper('mse', Y_pred, Y_gt)
    mrmse, mrmse_std = evaluation_count:eval_for_paper('mrmse', Y_pred, Y_gt)
    mmae, mmae_std = evaluation_count:eval_for_paper('mmae', Y_pred, Y_gt)
    print(string.format('Test Count Loss: split(%s)  mrmse: %f std: %f, mmae: %f std: %f, mse: %f std: %f', split, mrmse, mrmse_std, mmae, mmae_std, mse, mse_std))
	end
  return mse, mrmse, mmae
end

local earlystop = opt.estop
local min_loss = 10000
local min_ep = 0

lightmodel = model:clone()
lightmodel:clearState()

local i = 1
while(1) do
  if i % opt.checkpt == 0 or i == n_epochs then
    --local lightmodel = model:clone()
    --lightmodel:clearState()
    --torch.save(exp_dir .. '/' .. 'cnn_count_' .. 'ep_' .. tostring(i) .. '.t7', lightmodel)
    print('-----------')
    local mse, mrmse, mmae = test(model, 'test')
    print('-----------')
  end
  if i == n_epochs then
    break
  end
  local t_l = train()
  local Mse, Mrmse, Mmmae = test(model, 'val')
  local v_l = Mse
  if min_loss > v_l then
    min_loss = v_l
    min_ep = i
    lightmodel = model:clone()
    lightmodel:clearState()
    --torch.save(exp_dir .. '/counting_best.t7', lightmodel)
  end
  if i > 2 then
    logger:add{t_l, v_l}
    table.insert(tr_loss, t_l)
    table.insert(vl_loss, v_l)
    logger.showPlot = false
    logger.epsfile = '/home/prithv1/public_html/torch_logs/' .. exp_dir .. '.eps'
    logger:style{'-', '-'}
    logger:plot()
    if i~=1 then
      os.execute('sh /home/prithv1/public_html/torch_logs/convert.sh')
    end
  end
  if v_l > min_loss and (i-min_ep) == earlystop then
    break
  end
  i = i + 1
end
torch.save(exp_dir .. '/counting_best.t7', lightmodel)
model_new = torch.load(exp_dir .. '/counting_best.t7')
local MSE, MRMSE, MMAE = test(model_new, 'test', opt.bsize)
local best_losses = {}
best_losses['Test Count MSE'] = MSE
best_losses['Test Count MRMSE'] = MRMSE
best_losses['Test Count MMAE'] = MMAE
local loss_json = json.encode(best_losses)
local l_file = io.open(exp_dir .. '/test_results.json', 'w')
if l_file then
  l_file:write(loss_json)
  io.close(l_file)
end
print('---------')
print('---------')
print('---------')










