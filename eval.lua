--
--------------------------------------------------------------------------------
--         File:  eval.lua
--  Description:  Evaluation utilities for counting
--       Author:  Ramakrishna Vedantam (vrama91), <vrama91@vt.edu>
-- Organization:  Computer Vision Lab, Virginia Tech
--      Created:  03/08/2016
--------------------------------------------------------------------------------
--

-- The script takes raw counts from a method, thresholds them to 0 and rounds them
-- to get the final count predictions. After that we can compute either the MRMSE
-- or the MMAE metrics

local eval = torch.class('eval_count')

function eval:__init(relu, round)
	self.relu = relu or 1
	self.round = round or 1
end

function eval:_prepro(count_pred)
	local counts = count_pred:clone()

	-- threshold counts at 0
	if self.relu == 1 then	
		counts:cmax(0)
	end

	-- round counts
	if self.round == 1 then
		counts = torch.round(counts)
	end

	return counts
end

function eval:eval_for_paper(method, count_pred, targets, trials)
	local trials = trials or 10
	local splits = 0.8	

	local res = torch.Tensor(trials)

	for i = 1, trials do
		pm = torch.multinomial(torch.range(1, count_pred:size(1)), splits * count_pred:size(1), true)
		count_trial = count_pred:index(1, pm)
		target_trial = targets:index(1, pm)

		if method == 'mrmse' then
			res[i] = eval:mrmse(count_trial, target_trial)
		elseif method == 'mse' then
			res[i] = eval:mse(count_trial, target_trial)
		elseif method == 'mmae' then
			res[i] = eval:mmae(count_trial, target_trial)
		end
	end

	return res:mean(), res:std()
end

function eval:mrmse(count_pred, targets)
	count_pred = count_pred:double()
	targets = targets:double()

	count_pred = self:_prepro(count_pred)

	local mrmse = torch.pow(count_pred - targets, 2)
	mrmse = torch.mean(mrmse, 1)
	mrmse = torch.sqrt(mrmse)
	mrmse = torch.mean(mrmse)

	return mrmse
end

function eval:mse(count_pred, targets)
	count_pred = count_pred:clone() 

	targets = targets:double()
	count_pred = count_pred:double()

	local mse = torch.pow(targets - count_pred, 2):mean()
	return mse
end

function eval:mmae(count_pred, targets)
	count_pred = count_pred:double()
	targets = targets:double()

	count_pred = self:_prepro(count_pred)

	local mmae = torch.abs(targets-count_pred):mean()

	return mmae
end

